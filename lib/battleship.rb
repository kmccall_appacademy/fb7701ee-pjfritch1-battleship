class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @board = board
    @player = player
  end

  def attack(tile)
    board[tile] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    play = @player.get_play

    attack(play)
  end
end
