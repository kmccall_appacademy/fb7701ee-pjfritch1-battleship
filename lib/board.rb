class Board
  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  attr_accessor :grid

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    @grid[row][col] = val
  end


  def self.default_grid
    row = []
    10.times do
      row << nil
    end

    default_grid = []
    10.times do
      default_grid << row
    end

    default_grid
  end

  def count
    ships = 0
    @grid.each do |row|
      row.each do |tile|
        ships += 1 if tile == :s
      end
    end

    ships
  end

  def empty?(space = "hogwash")
    if space.class == Array

      return false if @grid[space[0]][space[1]] != nil

    else
      @grid.each { |row| return false unless row.all? {|tile| tile != :s} }
    end

    true
  end

  def full?
    @grid.each do |row|
      return false unless row.all? {|tile| tile == :s}
    end

    true
  end

  def place_random_ship

    if self.full?
      raise("board is full")
    else
      row, col = rand(@grid.length), rand(@grid.length)
      @grid[row][col] = :s
    end
  end

  def won?
    return true if self.empty?

    false
  end

end
